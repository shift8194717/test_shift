from src.routers.salary.router import router as salary_router
from src.routers.employee.router import router as employee_router
from src.auth.router import router as auth_router

from fastapi import FastAPI
import uvicorn

salary_project = FastAPI(title="Salary Project", version="1.0")

routers: tuple = (
    auth_router,
    salary_router,
    employee_router
)

for router in routers:
    salary_project.include_router(router)


if __name__ == "__main__":
    uvicorn.run(salary_project, host="0.0.0.0", port=8000)
