import os
from dotenv import load_dotenv

load_dotenv()

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_NAME = os.environ.get("DB_NAME")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
AUTH_SECRET_KEY = os.environ.get("SECRET_KEY")
AUTH_ALGORITHM = os.environ.get("ALGORITHM")
AUTH_EXPIRATION_TIME = int(os.environ.get("AUTH_EXPIRATION_TIME"))
