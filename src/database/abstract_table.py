from abc import abstractmethod, ABC
from typing import Union

import abstract as abstract
from sqlalchemy import Table, Select, Update, Delete
from sqlalchemy.ext.asyncio import AsyncSession


class AbstractTable(ABC):
    table: abstract = Table

    @classmethod
    async def execute_statement(cls, statement: Union[Select, Update, Delete] = ...,
                                session: AsyncSession = ...) -> bool:
        try:
            res = await session.execute(statement)
            await session.commit()
            if res.fetchall():
                return True
            else:
                return False
        except Exception as e:
            await session.rollback()
            raise e

    @classmethod
    async def execute_query(cls, query: Select = ..., session: AsyncSession = ...) -> list[dict]:
        try:
            res = await session.execute(query)
            rows = res.fetchall()
            keys = res.keys()
            return [dict(zip(keys, r)) for r in rows]
        except Exception as e:
            await session.rollback()
            raise e

    @abstractmethod
    async def get_one(self):
        pass

    @abstractmethod
    async def get_list(self):
        pass
