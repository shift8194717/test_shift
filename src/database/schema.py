from uuid import UUID

from pydantic import BaseModel, Field


class UpdatePasswordModel(BaseModel):
    employee_id: UUID = Field(...),
    password: str = Field(...)
