from passlib.handlers.pbkdf2 import pbkdf2_sha256
from sqlalchemy import MetaData, Table, Column, and_, select, update
from sqlalchemy.dialects.postgresql import UUID, VARCHAR, BOOLEAN
from sqlalchemy.ext.asyncio import AsyncSession

from src.auth.schema import EmployeeHashModel
from src.database.abstract_table import AbstractTable
from src.database.schema import UpdatePasswordModel


class Employee(AbstractTable):
    table = Table(
        "employee",
        MetaData(schema="public"),
        Column("uu_id", UUID, primary_key=True),
        Column("vc_fname", VARCHAR, nullable=False),
        Column("vc_sname", VARCHAR, nullable=False),
        Column("is_admin", BOOLEAN, nullable=False, default=False),
        Column("vc_login", VARCHAR, nullable=False),
        Column("vc_pwdhash", VARCHAR)
    )

    id = table.c.uu_id
    fname = table.c.vc_fname
    sname = table.c.vc_sname
    is_admin = table.c.is_admin
    login = table.c.vc_login
    pwdhash = table.c.vc_pwdhash

    id_label = id.label("id")
    fname_label = fname.label("first_name")
    sname_label = sname.label("second_name")
    is_admin_label = is_admin.label("is_admin")
    login_label = login.label("login")
    pwdhash_label = pwdhash.label("password_hash")

    async def get_one(self, db: AsyncSession = ..., employee_id: UUID = ...) -> dict:
        """
        Возвращает информацию о сотруднике.
        """
        one_query = await self.execute_query(
            query=select(
                    self.id_label,
                    self.fname_label,
                    self.sname_label,
                    self.login_label,
                    self.pwdhash_label,
                    self.is_admin_label
                ).where(
                    and_(self.id == employee_id)
                ),
            session=db
        )
        return one_query[0] if one_query else {}

    async def get_list(self, db: AsyncSession = ...) -> list[dict]:
        """
        Возвращает список всех сотрудников.
        """
        list_query = await self.execute_query(
            query=select(
                    self.id_label,
                    self.fname_label,
                    self.sname_label,
                    self.login_label,
                    self.pwdhash_label,
                    self.is_admin_label
                ).order_by(
                    self.sname,
                    self.fname
                ),
            session=db
        )
        return list_query

    async def get_auth_info(self, db: AsyncSession = ..., employee_login: str = ...) -> dict:
        """
        Получение информации для авторизации
        Возвращается словарь:
            {
                "id": UUID,
                "is_admin": bool,
                "hash": str
            }
            или {} в случае, если нет такого пользователя
        """
        one_query = await self.execute_query(
            query=select(
                    self.id_label,
                    self.is_admin_label,
                    self.pwdhash_label
                ).where(
                    and_(self.login == employee_login)
                ),
            session=db
        )
        return one_query[0] if one_query else {}

    async def update_password(self, db: AsyncSession = ..., update_data: UpdatePasswordModel = ...) -> bool:
        """
        Обновление пароля
        """
        hashed_password = EmployeeHashModel(vc_pwdhash=pbkdf2_sha256.hash(update_data.password))
        update_stmt = \
            await self.execute_statement(
                statement=update(
                            self.table
                        ).values(
                            hashed_password.model_dump()
                        ).where(
                            and_(self.id == update_data.employee_id)
                        ).returning(
                            self.id
                        ),
                session=db
            )
        return update_stmt


employee = Employee()
