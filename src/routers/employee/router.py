from typing import Union, List, Dict, Any
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from src.auth.router import oauth2_scheme
from src.auth.util import token_decode
from src.database.async_session import db_async_session
from src.routers.employee.model import employee

router: APIRouter = APIRouter(
    prefix="/employee",
    tags=["Сотрудники"]
)


@router.get("/all")
async def get_employee_list(
        token: str = Depends(oauth2_scheme),
        db: AsyncSession = Depends(db_async_session)
) -> List[Dict[str, Any]]:
    """
    Возвращает список сотрудников \n
    Доступно только для администратора \n
    Возвращает список словарей вида: \n
        [
            {
                "id": UUID,
                "first_name": str,
                "second_name": str,
                "login": str,
                "password_hash": str,
                "is_admin": bool
            }
        ]
    """
    _, is_admin = await token_decode(token)

    if not is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Permission denied")

    employee_list = await employee.get_list(db)
    return employee_list


@router.get("/me")
async def get_employee_me(
        token: str = Depends(oauth2_scheme),
        db: AsyncSession = Depends(db_async_session)
) -> Dict[str, Any]:
    """
    Возвращает информацию по сотруднику \n
    Возвращает словарь вида: \n
        {
            "id": UUID,
            "first_name": str,
            "second_name": str,
            "login": str,
            "password_hash": str,
            "is_admin": bool
        }
    """
    user_id, _ = await token_decode(token)

    employee_me: dict = await employee.get_one(db, user_id)
    if not employee_me:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Employee not found")

    return employee_me


@router.get("/{employee_id}")
async def get_employee_one(
        token: str = Depends(oauth2_scheme),
        db: AsyncSession = Depends(db_async_session),
        employee_id: Union[UUID, None] = ...
) -> Dict[str, Any]:
    """
    Возвращает информацию по сотруднику \n
    Возвращает словарь вида: \n
        {
            "id": UUID,
            "first_name": str,
            "second_name": str,
            "login": str,
            "password_hash": str,
            "is_admin": bool
        }
    """
    user_id, is_admin = await token_decode(token)
    if not (is_admin or user_id == employee_id):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Permission denied")

    employee_one: dict = await employee.get_one(db, employee_id)
    if not employee_one:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Employee not found")

    return employee_one
