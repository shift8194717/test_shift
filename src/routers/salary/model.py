from sqlalchemy import MetaData, Table, Column, and_, select, ForeignKeyConstraint
from sqlalchemy.dialects.postgresql import UUID, DATE, NUMERIC
from sqlalchemy.ext.asyncio import AsyncSession

from src.database.abstract_table import AbstractTable
from src.routers.employee.model import employee


class Salary(AbstractTable):
    table = Table(
        "salary",
        MetaData(schema="public"),
        Column("uu_id", UUID, primary_key=True),
        Column("uu_employee_id", UUID, nullable=False),
        Column("n_salary", NUMERIC, nullable=False),
        Column("d_increase_at", DATE, nullable=False),
        Column("n_percent", NUMERIC, nullable=False),
        ForeignKeyConstraint(["uu_employee_id"], ["employee.uu_id"])
    )

    id = table.c.uu_id
    employee_id = table.c.uu_employee_id
    salary = table.c.n_salary
    increase_at = table.c.d_increase_at
    percent = table.c.n_percent

    id_label = id.label("id")
    employee_id_label = employee_id.label("employee_id")
    salary_label = salary.label("salary")
    increase_at_label = increase_at.label("increase_at")
    percent_label = percent.label("percent")

    async def get_one(self, db: AsyncSession = ..., employee_id: UUID = ...) -> dict:
        """
        Возвращает информацию о сотруднике.
        """
        one_query = await self.execute_query(
            query=select(
                self.id_label,
                self.employee_id_label,
                employee.sname_label,
                employee.fname_label,
                self.salary_label,
                self.increase_at_label,
                self.percent_label
            ).join(
                employee.table,
                and_(self.employee_id == employee.id)
            ).where(
                and_(self.employee_id == employee_id)
            ),
            session=db
        )
        return one_query[0] if one_query else {}

    async def get_list(self, db: AsyncSession = ...) -> list[dict]:
        """
        Возвращает список всех сотрудников.
        """
        list_query = await self.execute_query(
            query=select(
                        self.id_label,
                        self.employee_id_label,
                        employee.sname_label,
                        employee.fname_label,
                        self.salary_label,
                        self.increase_at_label,
                        self.percent_label
                    ).join(
                        employee.table,
                        and_(self.employee_id == employee.id)
                    ).order_by(
                        employee.sname_label,
                        employee.fname_label
                    ),
            session=db
        )
        return list_query


salary = Salary()
