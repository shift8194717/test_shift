from typing import Union, Dict, Any, List
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from src.auth.router import oauth2_scheme
from src.auth.util import token_decode
from src.database.async_session import db_async_session
from src.routers.salary.model import salary

router: APIRouter = APIRouter(
    prefix="/salary",
    tags=["Salary"]
)


@router.get("/all")
async def get_salary_list(
        token: str = Depends(oauth2_scheme),
        db: AsyncSession = Depends(db_async_session)
) -> List[Dict[str, Any]]:
    """
    Возвращает список всех зарплат по сотрудникам, с информацией по повышению \n
    Доступно только для администратора \n
    Возвращает список словарей вида: \n
        [
            {
                "id": UUID,
                "employee_id": UUID,
                "second_name": str,
                "first_name": str,
                "salary": numeric(20, 0),
                "increase_at": date,
                "percent": numeric(4, 1)
            }
        ]
    """
    _, is_admin = await token_decode(token)
    if not is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Permission denied")
    salary_list = await salary.get_list(db)
    return salary_list


@router.get("/me")
async def get_salary_me(
        token: str = Depends(oauth2_scheme),
        db: AsyncSession = Depends(db_async_session)
) -> Dict[str, Any]:
    """
    Возвращает информацию о зарплате сотрудника, с информацией по повышению \n
    Возвращает словарь вида: \n
        {
            "id": UUID,
            "employee_id": UUID,
            "second_name": str,
            "first_name": str,
            "salary": numeric(20, 0),
            "increase_at": date,
            "percent": numeric(4, 1)
        }
    """
    user_id, _ = await token_decode(token)

    salary_me = await salary.get_one(db, user_id)
    if not salary_me:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Employee not found")

    return salary_me


@router.get("/{employee_id}")
async def get_salary_one(
        token: str = Depends(oauth2_scheme),
        db: AsyncSession = Depends(db_async_session),
        employee_id: Union[UUID, None] = ...
) -> Dict[str, Any]:
    """
    Возвращает информацию о зарплате сотрудника, с информацией по повышению \n
    Достп для самого сотрудника и админа \n
    Возвращает словарь вида: \n
        {
            "id": UUID,
            "employee_id": UUID,
            "second_name": str,
            "first_name": str,
            "salary": numeric(20, 0),
            "increase_at": date,
            "percent": numeric(4, 1)
        }
    """
    user_id, is_admin = await token_decode(token)

    if not (is_admin or (user_id == employee_id)):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Permission denied")

    salary_one = await salary.get_one(db, employee_id)
    if not salary_one:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Employee not found")

    return salary_one
