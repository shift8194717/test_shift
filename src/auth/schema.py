from pydantic import BaseModel, Field


class EmployeeHashModel(BaseModel):
    vc_pwdhash: str = Field(...)
