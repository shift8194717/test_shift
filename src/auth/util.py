from uuid import UUID

import jwt
from fastapi import HTTPException
from starlette import status

from config import AUTH_SECRET_KEY, AUTH_ALGORITHM


async def token_decode(token: str = ...):
    try:
        payload = jwt.decode(jwt=token, key=AUTH_SECRET_KEY, algorithms=[AUTH_ALGORITHM], verify_exp=True)
    except jwt.ExpiredSignatureError:
        raise HTTPException(status_code=401, detail="Token has expired", headers={"WWW-Authenticate": "Bearer"})
    except jwt.DecodeError:
        raise HTTPException(status_code=401, detail="Invalid token", headers={"WWW-Authenticate": "Bearer"})

    employee_id = payload.get("employee_id")
    is_admin = payload.get("is_admin")
    if employee_id is None or is_admin is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Invalid token")

    return UUID(employee_id), is_admin
