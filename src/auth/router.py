from datetime import datetime, timedelta

import jwt
from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.handlers.pbkdf2 import pbkdf2_sha256
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from config import AUTH_SECRET_KEY, AUTH_ALGORITHM, AUTH_EXPIRATION_TIME
from src.database.async_session import db_async_session
from src.database.schema import UpdatePasswordModel
from src.routers.employee.model import employee

router: APIRouter = APIRouter(
    prefix="/auth",
    tags=["Authentication"],
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/login")


@router.post("/login")
async def auth_check_access(
        db: AsyncSession = Depends(db_async_session),
        login_data: OAuth2PasswordRequestForm = Depends()
) -> dict:
    employee_info = await employee.get_auth_info(db, login_data.username)

    if not employee_info:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User not found",
            headers={"WWW-Authenticate": "Bearer"}
        )

    if employee_info["password_hash"] is None:
        await employee.update_password(db, UpdatePasswordModel(employee_id=employee_info["id"],
                                                               password=login_data.password))
        employee_info = await employee.get_auth_info(db, login_data.username)

    if not pbkdf2_sha256.verify(login_data.password, employee_info["password_hash"]):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid credentials",
            headers={"WWW-Authenticate": "Bearer"}
        )
    return \
        {
            "access_token": jwt.encode(
                payload={
                    "employee_id": str(employee_info["id"]),
                    "is_admin": employee_info["is_admin"],
                    "hashed_password": employee_info["password_hash"],
                    "exp": datetime.utcnow() + timedelta(minutes=AUTH_EXPIRATION_TIME)
                },
                key=AUTH_SECRET_KEY,
                algorithm=AUTH_ALGORITHM
            ),
            "token_type": "bearer"
        }
